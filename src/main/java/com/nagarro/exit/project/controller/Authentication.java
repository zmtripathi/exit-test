package com.nagarro.exit.project.controller;

import com.nagarro.exit.project.models.Token;
import com.nagarro.exit.project.models.User;
import com.nagarro.exit.project.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/exit")
public class Authentication {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(value = "/login")
    public ResponseEntity<?> loginUser(@RequestBody User user) {
        return ResponseEntity.ok(new Token(authenticationService.loginUser(user)));
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<?> signupUser(@RequestBody User user) {
        return ResponseEntity.ok(authenticationService.signUpUser(user));
    }
}
