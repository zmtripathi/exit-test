package com.nagarro.exit.project.services;

import com.nagarro.exit.project.models.User;

public interface AuthenticationService {
    String loginUser(User user);
    User signUpUser(User user);
}
