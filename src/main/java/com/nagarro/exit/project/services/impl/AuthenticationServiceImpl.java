package com.nagarro.exit.project.services.impl;

import com.google.gson.Gson;
import com.nagarro.exit.project.models.User;
import com.nagarro.exit.project.repository.UserRepository;
import com.nagarro.exit.project.services.AuthenticationService;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private UserRepository userRepository;
    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    long nowMillis = System.currentTimeMillis();
    Date now = new Date(nowMillis);
    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("ZXhpdA==");
    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
    long ttlMillis = 10000L;

    @Override
    public String loginUser(User user) {
        List<User> users = userRepository.findByEmail(user.getEmail());
        log.info("User {}", new Gson().toJson(users));
        if(users.size() == 1) {
            User authUser = users.get(0);
            Boolean isAuth = authUser.getPassword().equals(user.getPassword());
            log.info("Auth {}", new Gson().toJson(isAuth));
            if(isAuth) {
                JwtBuilder builder = Jwts.builder().setId(String.valueOf(user.getId()))
                        .setIssuedAt(now)
                        .setSubject("email")
                        .setIssuer(user.getEmail())
                        .signWith(signatureAlgorithm, signingKey);
                if (ttlMillis >= 0) {
                    long expMillis = nowMillis + ttlMillis;
                    Date exp = new Date(expMillis);
                    builder.setExpiration(exp);
                }
                return builder.compact();
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public User signUpUser(User user) {
        return userRepository.save(user);
    }
}
